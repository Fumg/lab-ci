#include "integer_avg.h"
#include "unity.h"

#include <stdlib.h>
#include <string.h>

void setUp(void)
{
    /* Set stuff up here */
}

void tearDown(void)
{
    /* Clean stuff up here */
}

void test_sum_function(void)
{
    /* Check result */
    TEST_ASSERT_EQUAL_INT_MESSAGE(1, integer_avg(1, 1), "Error in integer_avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(50, integer_avg(50, 50), "Error in integer_avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(-20, integer_avg(10, -50), "Error in integer_avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(25, integer_avg(0, 50), "Error in integer_avg");
	
    //TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_avg(100, 100), "Error in integer_avg");
}

